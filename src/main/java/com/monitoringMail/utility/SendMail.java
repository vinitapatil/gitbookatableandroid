package com.monitoringMail.utility;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.MultiPartEmail;
import org.apache.commons.mail.SimpleEmail;

public class SendMail {

	/*public static void main(String[] args) throws AddressException, MessagingException, EmailException {

		MonitoringMail mail = new MonitoringMail();
		mail.sendMail(MailConfig.server, MailConfig.from, MailConfig.to, MailConfig.subject, MailConfig.messageBody,
				MailConfig.attachmentPath, MailConfig.attachmentName);

		System.out.println("Sending Mail.....");
		sendEmail();
		
		System.out.println("Email sent..");
		Email email = new SimpleEmail();
		email.setHostName("smtp.gmail.com");
		email.setSmtpPort(587);
		email.setAuthenticator(new DefaultAuthenticator("vinitapatil816@gmail.com", "myangel11011987"));
		email.setSSL(true);
		email.setFrom("vinitapatil816@gmail.com");
		email.setSubject("Automation Test Mail");
		email.setMsg("This is a test mail ... :-)");
		email.addTo("patilwinee@gmail.com");
		email.send();
	}*/

	public static void sendEmail() throws EmailException {
		// Create the attachment
		  EmailAttachment attachment = new EmailAttachment();
		  attachment.setPath("./extentreport/AutomationReport.html");
		  attachment.setDisposition(EmailAttachment.ATTACHMENT);
		  attachment.setDescription("Automation Test Mail");
		  attachment.setName("Automation Test Report");

		  // Create the email message
		  MultiPartEmail email = new MultiPartEmail();
		  email.setHostName("smtp.gmail.com");
		  email.setSmtpPort(587);
			email.setAuthenticator(new DefaultAuthenticator("vinitapatil816@gmail.com", "myangel11011987"));
			email.setSSL(true);
		  email.addTo("patilwinee@gmail.com", "John Doe");
		  email.setFrom("vinitapatil816@gmail.com", "Me");
		  email.setSubject("Automation Test Mail");
		  email.setMsg("Find the attached automation test report. ");

		  // add the attachment
		  email.attach(attachment);

		  // send the email
		  email.send();
	}

}
