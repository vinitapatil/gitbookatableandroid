package com.PageObjects;

import org.openqa.selenium.By;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;

public class MyBookingPageiOS extends MyBookingPage{
	private IOSDriver<MobileElement> driver;
	
		public MyBookingPageiOS(AppiumDriver<MobileElement> driver) {
					
		this.driver = (IOSDriver<MobileElement>) driver;
		}
		
		public By search_tab = By.id("Search");
		
		public By bookings_tab = By.id("Bookings");
		
		public By find_a_restaurant_in_bookings = By.id("");
		
		public By upcoming_tab = By.xpath("//XCUIElementTypeSegmentedControl[1]/XCUIElementTypeButton[1]");
		
		public By past_tab = By.xpath("//XCUIElementTypeSegmentedControl[1]/XCUIElementTypeButton[2]");
		
		public By first_booking_record = By.className("XCUIElementTypeOther");
		
		public By booked_restaurant_name = By.xpath("//XCUIElementTypeOther[1]/XCUIElementTypeStaticText[1]");
		
		public By booked_restaurant_detail = By.xpath("//XCUIElementTypeOther[1]/XCUIElementTypeStaticText[2]");
		
		public By next_arrow = By.id("rightDisclosure");
		
		public By booked_restaurant_name_detail_page = By.id("XCUIElementTypeStaticText");
		
		public By person_text = By.className("XCUIElementTypeStaticText");
		
		public By date_text = By.className("XCUIElementTypeStaticText");
		
		public By time_text = By.className("XCUIElementTypeStaticText");
		
		public By cancel_booking = By.className("XCUIElementTypeButton");

		
		@Override
		public boolean tapOnSearchTab() 
		{
			try 
			{
				MobileElement search_tab_element = driver.findElement(search_tab);
				search_tab_element.click();
				
				return true;
			}
			catch (Exception e) 
			{
				System.out.println("Search tab was not found");
				return false;
			}
		}
		
		@Override
		public boolean tapOnBookingsTab() 
		{
			try 
			{
				MobileElement search_tab_element = driver.findElement(bookings_tab);
				search_tab_element.click();
				
				return true;
			}
			catch (Exception e) 
			{
				System.out.println("Select People Dropdown was not found");
				return false;
			}
		}
		@Override
		public boolean tapOnFindARestaurantButton() 
		{
			try 
			{
				MobileElement search_tab_element = driver.findElement(find_a_restaurant_in_bookings);
				search_tab_element.click();
				
				return true;
			}
			catch (Exception e) 
			{
				System.out.println("Select People Dropdown was not found");
				return false;
			}
		}
		@Override
		public boolean tapOnUpcomingTab() 
		{
			try 
			{
				MobileElement search_tab_element = driver.findElement(upcoming_tab);
				search_tab_element.click();
				
				return true;
			}
			catch (Exception e) 
			{
				System.out.println("Select People Dropdown was not found");
				return false;
			}
		}
		@Override
		public boolean tapOnPastTab() 
		{
			try 
			{
				MobileElement search_tab_element = driver.findElement(past_tab);
				search_tab_element.click();
				
				return true;
			}
			catch (Exception e) 
			{
				System.out.println("Select People Dropdown was not found");
				return false;
			}
		}
		@Override
		public boolean selectFirstBookingRecord() 
		{
			try 
			{
				MobileElement search_tab_element = driver.findElement(first_booking_record);
				search_tab_element.click();
				
				return true;
			}
			catch (Exception e) 
			{
				System.out.println("Select People Dropdown was not found");
				return false;
			}
		}
		@Override
		public boolean getBookedRestaurantName() 
		{
			try 
			{
				MobileElement search_tab_element = driver.findElement(booked_restaurant_name);
				search_tab_element.click();
				
				return true;
			}
			catch (Exception e) 
			{
				System.out.println("Select People Dropdown was not found");
				return false;
			}
		}
		@Override
		public boolean getBookedRestaurantDetails() 
		{
			try 
			{
				MobileElement search_tab_element = driver.findElement(booked_restaurant_detail);
				search_tab_element.click();
				
				return true;
			}
			catch (Exception e) 
			{
				System.out.println("Select People Dropdown was not found");
				return false;
			}
		}
		@Override
		public boolean tapOnNextArrow() 
		{
			try 
			{
				MobileElement search_tab_element = driver.findElement(next_arrow);
				search_tab_element.click();
				
				return true;
			}
			catch (Exception e) 
			{
				System.out.println("Select People Dropdown was not found");
				return false;
			}
		}
		@Override
		public boolean getRestaurantNameInDetailsPage() 
		{
			try 
			{
				MobileElement search_tab_element = driver.findElement(booked_restaurant_name_detail_page);
				search_tab_element.click();
				
				return true;
			}
			catch (Exception e) 
			{
				System.out.println("Select People Dropdown was not found");
				return false;
			}
		}
		@Override
		public boolean getPersonTextInDetailsPage() 
		{
			try 
			{
				MobileElement search_tab_element = driver.findElement(person_text);
				search_tab_element.click();
				
				return true;
			}
			catch (Exception e) 
			{
				System.out.println("Select People Dropdown was not found");
				return false;
			}
		}
		@Override
		public boolean getDateTextInDetailsPage() 
		{
			try 
			{
				MobileElement search_tab_element = driver.findElement(date_text);
				search_tab_element.click();
				
				return true;
			}
			catch (Exception e) 
			{
				System.out.println("Select People Dropdown was not found");
				return false;
			}
		}
		@Override
		public boolean getTimeTextInDetailsPage() 
		{
			try 
			{
				MobileElement search_tab_element = driver.findElement(time_text);
				search_tab_element.click();
				
				return true;
			}
			catch (Exception e) 
			{
				System.out.println("Select People Dropdown was not found");
				return false;
			}
		}
		@Override
		public boolean tapOnCancelBookingsButton() 
		{
			try 
			{
				MobileElement search_tab_element = driver.findElement(cancel_booking);
				search_tab_element.click();
				
				return true;
			}
			catch (Exception e) 
			{
				System.out.println("Select People Dropdown was not found");
				return false;
			}
		}
		
}
