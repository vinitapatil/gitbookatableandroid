package com.PageObjects;

import java.util.HashMap;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;

/** Created by Vinita Patil  */
public class RestaurantDetailsPageiOS extends RestaurantDetailsPage
{
	private IOSDriver<MobileElement> driver;
	private JavascriptExecutor js;
	public RestaurantDetailsPageiOS(AppiumDriver<MobileElement> driver) {
		this.driver = (IOSDriver<MobileElement>) driver;
		js = (JavascriptExecutor) driver;
		}	
	
	  public By restaurant_name_label = By.id("restaurant-name-label");
	  
	  public By restaurant_details_opening_hours_title_label = By.id("restaurant-details.opening-hours-title-label");
	 
	  public By restaurant_details_opening_hours_value_label = By.id("restaurant-details.opening-hours-value-label");
	  
	  public By restaurant_details_description_title_label = By.id("restaurant-details.description-title-label");
	 
	  public By restaurant_details_description_value_label = By.id("restaurant-details.description-value-label");
	  
	  public By restaurant_details_phone_value_label = By.id("restaurant-details.enquiries-title-label");
		  
	  public By info_tab_button = By.id("info-tab-button");
	  
	  public By contact_tab_button = By.id("location-tab-button");

	  public By book_Now_button = By.id("book-button");
	  
	  public By back_button = By.id("navigation-back-arrow");
	  
	  public By contact_number_button = By.xpath("//XCUIElementTypeButton[starts-with(@name,'+')]");
	  
	  public By cancle_call = By.id("Cancel");

	  public By done_button = By.id("Done");
	  
	  //public By page_indicator = By.xpath("//XCUIElementTypeOther[@name='master view']/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypePageIndicator");
	  
	  public By page_indicator = By.className("XCUIElementTypePageIndicator");	  
	 
	  public By image_unavailable = By.id("Image unavailable");
	  
	  public By maps_view = By.xpath("//XCUIElementTypeOther[@name='master view']/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther");
	  
	  public By maps_view_blue_pin = By.xpath("//XCUIElementTypeButton[starts-with(@name,'GMS')]");
	  
	  public By text_on_maps_view_blue_pin = By.xpath("//XCUIElementTypeOther[starts-with(@name,'bubble')]");	  

	  public By image_view = By.xpath("//XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeCollectionView");
	  //public By image_view = By.id("carousel-image-view");
	  
	  @Override
	  public String getRestaurantNameLabel() {
			try {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				WebElement getRestaurantNameLabelElement = driver.findElement(restaurant_name_label);
				
				return getRestaurantNameLabelElement.getText();
			} catch (NoSuchElementException e) {
				System.out.println("Restaurant Name Label is not found");
			}
			
			return null;
		}  
	  
	  @Override
	  public String getContactText() {
			
			try {
				WebElement dialledNumberLabelelement = driver.findElement(contact_number_button);
				
				return dialledNumberLabelelement.getText();
			} catch (NoSuchElementException e) {
				System.out.println("Restaurant Name Label is not found");
			}
			
			return null;
		}
	  
	  @Override
	  public WebElement tapOnPhoneNumber() {			
			
				WebElement dialledNumberLabelelement = driver.findElement(contact_number_button);
				return dialledNumberLabelelement;	
					
		}
	  
	  @Override
	  public String getRestaurantDetailsOpeningHoursTitleLabel() {
			
			try {
			
				WebElement OpeningHoursTitleLabelelement = driver.findElement(restaurant_details_opening_hours_title_label);
				String OpeningHoursTitleLabel_actual =OpeningHoursTitleLabelelement.getText();
				return OpeningHoursTitleLabel_actual;
			
			} catch (NoSuchElementException e) {
				System.out.println("Restaurant Details Opening Hours Title Label is not found");
			}
			
			return null;
		}
	  
	  @Override
	  public String getRestaurantDetailsOpeningHoursValueLabel() {
			
			try {
				WebElement dialledNumberLabelelement = driver.findElement(restaurant_details_opening_hours_value_label);
				return dialledNumberLabelelement.getText();
			
			} catch (NoSuchElementException e) {
				System.out.println("Restaurant Details Opening Hours Value Label is not found");
			}
			
			return null;
		}
	  
	  @Override
	  public String getRestaurantDetailsDescriptionTitleLabel() {
			
			try {
				WebElement dialledNumberLabelelement = driver.findElement(restaurant_details_description_title_label);
				return dialledNumberLabelelement.getText();
			
			} catch (NoSuchElementException e) {
				System.out.println("Restaurant Details Description Title Label is not found");
			}
			
			return null;
		}
	  
	  @Override
	  public String getRestaurantDetailsDescriptionValueLabel() {
			
			try {
				WebElement dialledNumberLabelelement = driver.findElement(restaurant_details_description_value_label);
				return dialledNumberLabelelement.getText();
			
			} catch (NoSuchElementException e) {
				System.out.println("Restaurant Details Description Value Label is not found");
			}
			
			return null;
		}
	  
	  @Override
	  public String getRestaurantDetailsPhoneValueLabel() {
			
			try {
				WebElement restaurant_details_phone_value_label_element = driver.findElement(restaurant_details_phone_value_label);
				return restaurant_details_phone_value_label_element.getText();
			
			} catch (NoSuchElementException e) {
				System.out.println("Restaurant Details Description Value Label is not found");
			}
			
			return null;
		}
	  @Override
	  public boolean tapOnBookNowButtonfunction() 
		{
			try 
			{
				WebElement book_now_button_element = driver.findElement(book_Now_button);
				book_now_button_element.click();
				return true;
			}
			catch (Exception e) 
			{
				System.out.println("Book Now button was not found");
				return false;
			}
		}
	  @Override
	  public String getTextForBookNowButton() 
		{
			try 
			{
				WebElement book_now_button_element = driver.findElement(book_Now_button);
				return book_now_button_element.getText();
			}
			catch (Exception e) 
			{
				System.out.println("Book Now button was not found");
				return null;
			}
		}
	 	  
	  @Override
	  public boolean tapOnCancelCall() 
		{
			try 
			{
				WebElement cancel_call_element = driver.findElement(cancle_call);
				cancel_call_element.click();
				System.out.println("Cancel button on Call pop up has been clicked.");
				return true;
			}
			catch (Exception e) 
			{
				System.out.println("Cancel button on Call pop up was not found.");
				return false;
			}
		}
	  
	  @Override
	  public boolean tapOnInfoTab() 
		{
			try 
			{
				WebElement info_tab_element = driver.findElement(info_tab_button);
				info_tab_element.click();
				System.out.println("Info Tab has been clicked");
				return true;
			}
			catch (Exception e) 
			{
				System.out.println("Info Tab was not found");
				return false;
			}
		}
	  @Override
	  public String getTextForInfoTab() 
		{
			try 
			{
				WebElement info_tab_element = driver.findElement(info_tab_button);
				String info_tab_button_actual=info_tab_element.getText();
				return info_tab_button_actual;
			}
			catch (Exception e) 
			{
				System.out.println("Info Tab was not found");
				return null;
			}
		}
	  @Override
	  public boolean tapOnBackButton() 
		{
			try 
			{
				WebElement back_button_element = driver.findElement(back_button);
				back_button_element.click();
				return true;
			}
			catch (Exception e) 
			{
				System.out.println("Back Now button was not found");
				return false;
			}
		}
	  
	  @Override
	  public boolean tapOnDoneButton() 
		{
			try 
			{
				WebElement done_button_element = driver.findElement(done_button);
				done_button_element.click();
				return true;
			}
			catch (Exception e) 
			{
				System.out.println("DONE button was not found");
				return false;
			}
		}
	  @Override
	  public boolean tapOnMapView() 
		{
			try 
			{
				WebElement mapViewelement = driver.findElement(maps_view);
				mapViewelement.click();
				System.out.println("Map View has been clicked");
				return true;
			}
			catch (Exception e) 
			{
				System.out.println("Map View was not found");
				return false;
			}
		}
	  
	  @Override
	  public boolean tapOnMapViewBluePin() 
		{
			try 
			{
				WebElement mapViewelement = driver.findElement(maps_view_blue_pin);
				mapViewelement.click();
				System.out.println("Map View blue Pin has been clicked");
				return true;
			}
			catch (Exception e) 
			{
				System.out.println("Map View Blue Pin was not found");
				return false;
			}
		}
	  @Override
	  public String getTextOnMapView() 
		{
			try 
			{
				WebElement mapViewelement = driver.findElement(text_on_maps_view_blue_pin);
				return mapViewelement.getText();
			}
			catch (Exception e) 
			{
				System.out.println("Map View Blue Pin Restaurent Name was not found");
				return null;
			}			
		} 
	  
	  @Override
	  public boolean swipeImages() 
		{
		
		  try 
			{
			  
		  WebElement pageIndicator = driver.findElement(page_indicator);
		  String pageString= pageIndicator.getAttribute("value");
		  int length = pageString.length();
		  String count_string= pageString.substring(length-2, length).trim();

		  int count = Integer.parseInt(count_string);
		  System.out.println("Number of Image available to Swipe: "+count);
			   for (int i=0; i<=count; i++){
			 
		  JavascriptExecutor js = (JavascriptExecutor) driver;
		  HashMap<String, String> scrollObject = new HashMap<String, String>();
		  scrollObject.put("direction", "right");
		  js.executeScript("mobile: scroll", scrollObject);
		  
			   }
			   System.out.println("Swipe Successfully");
			}
			   catch (Exception e) 
				{
					System.out.println("Image swipe was not successfull");
				}	
			return false;
					
		}
	  @Override
	  public String getTextImageUnavailable() 
		{
			try 
			{
				WebElement image_unavailable_element = driver.findElement(image_unavailable);
				return image_unavailable_element.getText();
			}
			catch (Exception e) 
			{
				System.out.println("Map View Blue Pin Restaurent Name was not found");
				return null;
			}			
		} 
	  @Override
	  public boolean tapOnImageView() 
		{
			try 
			{
				WebElement imageViewelement = driver.findElement(image_view);
				imageViewelement.click();
				System.out.println("Image View has been clicked");
				return true;
			}
			catch (Exception e) 
			{
				System.out.println("Image View was not found");
				return false;
			}
		}
	  
	@Override
	public boolean tapOnContactTab() {
		
		try 
		{
			Thread.sleep(1000);
			WebElement contact_tab_element = driver.findElement(contact_tab_button);
			contact_tab_element.click();
			System.out.println("Contact Tab has been clicked");
			return true;
		}
		catch (Exception e) 
		{
			System.out.println("Contact Tab was not found");
			return false;
		}
	}
	@Override
	public String getTextForContactTab() {
		
		try 
		{
			WebElement contact_tab_element = driver.findElement(contact_tab_button);
			String contact_tab_button_actual =contact_tab_element.getText();
			return contact_tab_button_actual;
		}
		catch (Exception e) 
		{
			System.out.println("Contact Tab was not found");
			return null;
		}
	}
	@Override
	  public boolean swipeFromUpToBottom() 
		{
		
		  try 
			{		
		  JavascriptExecutor js = (JavascriptExecutor) driver;
		  HashMap<String, String> scrollObject = new HashMap<String, String>();
		  scrollObject.put("direction", "up");
		  js.executeScript("mobile: scroll", scrollObject);
		  System.out.println("Swipe up was Successfully done.");
			}
			   catch (Exception e) 
				{
					System.out.println("swipe up was not successfull");
				}	
			return false;
				
		}
	@Override
	  public boolean swipeFromBottomToUp() 
		{		
		  try 
		  {
			  
		  JavascriptExecutor js = (JavascriptExecutor) driver;
		  HashMap<String, String> scrollObject = new HashMap<String, String>();
		  scrollObject.put("direction", "down");
		  js.executeScript("mobile: scroll", scrollObject);
		  System.out.println("Swipe down was Successfully done");
			}
			   catch (Exception e) 
				{
					System.out.println("swipe down was not successfull");
				}	
			return false;
					
		}
}
