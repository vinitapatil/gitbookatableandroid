package com.PageObjects;

public abstract class SplashScreen{
	
	
	public abstract boolean permissionAllowButton();
	
	
	public abstract boolean permissionDenyButton();


	public abstract String getPermissionMessage();

	

}
