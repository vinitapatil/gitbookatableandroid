package com.PageObjects;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

/**
 * Created by Vinita Patil!
 *
 */
public class SplashScreenAndroid extends SplashScreen
{
	private AndroidDriver<MobileElement> driver;
	
	public SplashScreenAndroid(AppiumDriver<MobileElement> driver) {
		//this.driver = (AndroidDriver<MobileElement>) driver;
		PageFactory.initElements(new AppiumFieldDecorator((AndroidDriver<MobileElement>) driver), this);

		}
		
	  //public By permission_message = By.id("com.android.packageinstaller:id/permission_message");

	  //public By permission_allow_button = By.name("com.android.packageinstaller:id/permission_allow_button");
	
	  //public By permission_deny_button = By.id("com.android.packageinstaller:id/permission_deny_button");
		@AndroidFindBy(id="com.android.packageinstaller:id/permission_message")
		MobileElement permission_message;
		
		@AndroidFindBy(id="com.android.packageinstaller:id/permission_allow_button")
		MobileElement permission_allow_button;
		
		@AndroidFindBy(id="com.android.packageinstaller:id/permission_deny_button")
		MobileElement permission_deny_button;
		
	  @Override
	  public String getPermissionMessage() {
			
			try {
			
				return permission_message.getText();
			
			} catch (NoSuchElementException e) {
				System.out.println("Restaurant Details Opening Hours Title Label is not found");
			}
			
			return null;
		}

	
	  @Override
	public boolean permissionAllowButton() 
	{
		try 
		{
			permission_allow_button.click();
			return true;
		}
		catch (Exception e) 
		{
			System.out.println("Permission ALLOW button was not found");
			return false;
		}
	}
	  
	  @Override
	public boolean permissionDenyButton() 
	{
		try 
		{
			permission_deny_button.click();
			return true;
		}
		catch (Exception e) 
		{
			System.out.println("Permission DENY button was not found");
			return false;
		}
	}


	
}
