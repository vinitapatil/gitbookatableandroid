package com.PageObjects;

import org.openqa.selenium.WebElement;

public abstract class RestaurantDetailsPage{
	
	public abstract String getRestaurantNameLabel();

	public abstract String getRestaurantDetailsOpeningHoursTitleLabel();

	public abstract String getRestaurantDetailsOpeningHoursValueLabel();

	public abstract String getRestaurantDetailsDescriptionTitleLabel();

	public abstract String getRestaurantDetailsDescriptionValueLabel();

	public abstract boolean tapOnBookNowButtonfunction();

	public abstract boolean tapOnInfoTab();

	public abstract boolean tapOnContactTab();

	public abstract boolean tapOnBackButton();	
	
	public abstract boolean tapOnDoneButton();

	public abstract boolean tapOnMapView();

	public abstract boolean tapOnImageView();

	public abstract boolean tapOnCancelCall();

	public abstract String getTextOnMapView();

	public abstract boolean swipeImages();

	public abstract boolean tapOnMapViewBluePin();

	public abstract String getContactText();

	public abstract WebElement tapOnPhoneNumber();

	public abstract String getTextImageUnavailable();

	public abstract boolean swipeFromUpToBottom();

	public abstract boolean swipeFromBottomToUp();

	public abstract String getTextForInfoTab();

	public abstract String getTextForContactTab();

	public abstract String getTextForBookNowButton();

	public abstract String getRestaurantDetailsPhoneValueLabel();


	
}