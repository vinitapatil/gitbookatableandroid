package com.BookATable;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
//@Listeners(com.BookATable.Listener.class)	
public class TestSearchByLocation extends BaseTestInitializeClass{
	
	@Test(priority=8)
	public void searchByLocation() throws InterruptedException{
		//logger = report.startTest("TestSearchByRestaurant");
		//logger.log(Status.INFO, "Test Search By Restaurant");

		System.out.println("Landed on Restaurent Listing Screen");
		logger= report.createTest("Restaurant Listing Page : searchByLocation", "Verify functionality of search by Location.");
			if(restaurantNearMePage.searchTextbox() != null)
			{
				String searchLocation= "London";
				restaurantNearMePage.searchTextboxEdit();
				restaurantNearMePage.searchTextbox().sendKeys(searchLocation);
				System.out.println("Location Entered. "+searchLocation);
				restaurantNearMePage.HideKeyboard();
				System.out.println("Search result found : " +searchLocation +" is "+ +restaurantNearMePage.getDropdownCount());
				logger.log(Status.PASS, MarkupHelper.createLabel("Search result found for 1 character search.: " +searchLocation +" is "+ +restaurantNearMePage.getDropdownCount() ,ExtentColor.GREEN));
	
			}
			else
			{
				System.out.println("Search Text was not entered.");
				logger.log(Status.FAIL, MarkupHelper.createLabel("Failed to enter search text.", ExtentColor.RED));
				//utils.takeScreenshot("Search", "SearchTest");
			}
	}
		@Test(priority=9)
		public void selectFirstRecordForLocationSearch(){
			logger= report.createTest("Restaurant Listing Page :selectFirstRecordForLocationSearch", "Verify functionality of selecting first restaurant from the list.");

			if(restaurantNearMePage.selectFirstRecordForLocationSearch())
			{
				logger.log(Status.PASS, MarkupHelper.createLabel("Tapped on locatio from Location search Result.", ExtentColor.GREEN));
				System.out.println("Tapped on location from Location search Result.");
	
			}
			else
			{
				System.out.println("Failed to find the location from location search result.");
				logger.log(Status.FAIL, MarkupHelper.createLabel("Failed to find the location from location search result.", ExtentColor.RED));
	
				//utils.takeScreenshot("Search", "SearchTest");
			}
		}
		
		@Test(priority=10)
		public void verifyRestaurantNameAndAddress(){
			logger= report.createTest("Restaurant Listing Page :verifyRestaurantNameAndAddress", "Verify Restaurant Name and Address is present.");

			if(restaurantNearMePage.getrestaurantNameListView() != null)
				{
					System.out.println("Restaurant Name is :" +restaurantNearMePage.getrestaurantNameListView());	
					logger.log(Status.PASS, MarkupHelper.createLabel("Restaurant Name is :" +restaurantNearMePage.getrestaurantNameListView(), ExtentColor.GREEN));
		
				}
			else
				{
					System.out.println("Failed to find the Name of restaurant.");
					logger.log(Status.FAIL, MarkupHelper.createLabel("Failed to find the Name of restaurant.", ExtentColor.RED));
					//utils.takeScreenshot("Search", "SearchTest");
				}
			if(restaurantNearMePage.getrestaurantAddressListView() != null)
				{
					System.out.println("Restaurant Address is :" +restaurantNearMePage.getrestaurantAddressListView());	
					logger.log(Status.PASS, MarkupHelper.createLabel("Restaurant Address is :" +restaurantNearMePage.getrestaurantAddressListView(), ExtentColor.GREEN));
		
				}
			else
				{
					System.out.println("Failed to find the Address of restaurant.");
					logger.log(Status.FAIL, MarkupHelper.createLabel("Failed to find the Address of restaurant.", ExtentColor.RED));
					//utils.takeScreenshot("Search", "SearchTest");
				}
		}
		
		@Test(priority=11)
		public void verifyVerticalScrollInRestaurantList(){
			
			restaurantDetailsPage.swipeFromBottomToUp();
			
			restaurantDetailsPage.swipeFromBottomToUp();
				
			restaurantDetailsPage.swipeFromUpToBottom();
			
			restaurantDetailsPage.swipeFromUpToBottom();
		}
		@Test(priority=12)
		public void selectFirstRestaurantFromTheList(){		
			logger= report.createTest("Restaurant Listing Page :selectFirstRestaurantFromTheList", "Verify functionality of selecting first restaurant from the list.");

			if(restaurantNearMePage.selectFirstRestaurantFromTheList())
				{
					System.out.println("Tapped on Restaurent from Location search Result.");
					logger.log(Status.PASS, MarkupHelper.createLabel("Tapped on Restaurent from Location search Result.", ExtentColor.GREEN));
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				}
			else
				{
					System.out.println("Restaurent was not found to select.");
					logger.log(Status.FAIL, MarkupHelper.createLabel("Failed to find the restaurant.", ExtentColor.RED));
		
					//utils.takeScreenshot("Search", "SearchTest");
				}
		}
		/*@Test(priority=14)
		public void verifyVerticalScrollInDetailsPage(){
			if(restaurantDetailsPage.swipeFromBottomToUp())
				{
					System.out.println("Scroll down was successful.");	
					logger.log(Status.PASS, "Scroll down was successful.");
				}
			else
				{
					System.out.println("Failed to scroll down.");
					logger.log(Status.FAIL, "Failed to scroll down.");
					utils.takeScreenshot("Search", "SearchTest");
				}
			if(restaurantDetailsPage.swipeFromUpToBottom())
				{
					System.out.println("Scroll up was successful.");	
					logger.log(Status.PASS, "Scroll up was successful.");
				}
			else
				{
					System.out.println("Failed to scroll up.");
					logger.log(Status.FAIL, "Failed to scroll up.");
					utils.takeScreenshot("Search", "SearchTest");
				}
		}*/
	@Test(priority=13)
	public void tapOnContactTab() {
		logger= report.createTest("Restaurant Listing Page :tapOnContactTab", "Verify functionality of Contact tab.");

		if(restaurantDetailsPage.tapOnContactTab())
		{
			System.out.println("Tapped on Contact Tab.");
			logger.log(Status.PASS, MarkupHelper.createLabel("Tapped on Contact Tab.", ExtentColor.GREEN));

		}
		else
		{
			System.out.println("Contact Tab was not found.");
			logger.log(Status.FAIL, MarkupHelper.createLabel("Failed to tap on Contact Tab as it was not found.", ExtentColor.RED));
			//utils.takeScreenshot("Search", "SearchTest");
		}
	}
			
	@Test(priority=28)
	public void tapOnBookNowButton() throws InterruptedException{
		logger= report.createTest("Restaurant Listing Page :tapOnBookNowButton", "Verify functionality of tap on Book Now button.");

		if(restaurantDetailsPage.tapOnBookNowButtonfunction())
		{
			
			System.out.println("Clicked on Book Now Button.");
			logger.log(Status.PASS, MarkupHelper.createLabel("Clicked on Book Now Button.", ExtentColor.GREEN));
		}
		else
		{
			System.out.println("Book Now Button could not be clicked.");
			logger.log(Status.FAIL, MarkupHelper.createLabel("Failed to tap on Book Now.", ExtentColor.RED));

			//utils.takeScreenshot("Search", "SearchTest");
		}
	}
	@Test(priority=29)
	public void selectCalendarDateforBooking() throws InterruptedException{		
		logger= report.createTest("Restaurant Listing Page :selectCalendarDate", "Verify functionality of selection of date from calendar.");

		if(bdaScreen.selectCalendarDate())
		{			
			System.out.println("Diner has selected date from calendar.");
			logger.log(Status.PASS, MarkupHelper.createLabel("Diner has selected date from calendar.", ExtentColor.GREEN));

		}
		else
		{
			System.out.println("Failed to selectd date from the calendar.");
			logger.log(Status.FAIL, MarkupHelper.createLabel("Failed to selectd date from the calendar.", ExtentColor.RED));
			//utils.takeScreenshot("Search", "SearchTest");
		}

		if(bdaScreen.tapOnBookButton())
		{
			
			System.out.println("Clicked on Book Button");
			logger.log(Status.PASS, MarkupHelper.createLabel("Clicked on Book Now Button.", ExtentColor.GREEN));

		}
		else
		{
			System.out.println("Book Button could not be clicked.");
			logger.log(Status.FAIL, MarkupHelper.createLabel("Failed to tap on book button.", ExtentColor.RED));

			//utils.takeScreenshot("Search", "SearchTest");
		}
			
		if(bdaScreen.selectTimeSlot())
		{
			
			System.out.println("Time slot has been selected.");
			logger.log(Status.PASS, MarkupHelper.createLabel("Time slot has been selected.", ExtentColor.GREEN));

		}
		else
		{
			System.out.println("Time slot Button could not be clicked.");
			logger.log(Status.FAIL, MarkupHelper.createLabel("Failed to select the time slot.", ExtentColor.RED));

			//utils.takeScreenshot("Search", "SearchTest");
		}
		
		/*if(bdaScreen.entertextbox())
		{
			System.out.println("First Name has been Entered");
			logger.log(Status.PASS, "First Name has been Entered");

		}
		else
		{
			System.out.println("First name was not entered.");
			logger.log(Status.FAIL, "Failed to enter first Name.");

			utils.takeScreenshot("Search", "SearchTest");
		}*/
		bdaScreen.entertextbox();

	/*if(bdaScreen.tapOn_Close())
	{
		
		System.out.println("Clicked on Close Button");
		logger.log(Status.PASS, "Clicked on Close Button");

	}
	else
	{
		System.out.println("Close Button could not be clicked.");
		logger.log(Status.FAIL, "Failed to tap on Close button.");

		utils.takeScreenshot("Search", "SearchTest");
	}*/
		if(bdaScreen.tapOn_Continue_button())
		{
			
			System.out.println("Clicked on continue Button");
			logger.log(Status.PASS, MarkupHelper.createLabel("Clicked on continue Button", ExtentColor.GREEN));

		}
		else
		{
			System.out.println("Failed to tap on continue button.");
			logger.log(Status.FAIL, MarkupHelper.createLabel("Failed to tap on continue button.", ExtentColor.RED));

			//utils.takeScreenshot("Search", "SearchTest");
		}
		restaurantDetailsPage.swipeFromBottomToUp();
		if(bdaScreen.tapOn_Booking_Now_Confirm_button())
		{
			
			System.out.println("Clicked on Booking Now Button");
			logger.log(Status.PASS, MarkupHelper.createLabel("Clicked on Booking Now Button", ExtentColor.GREEN));

		}
		else
		{
			System.out.println("Failed to tap on Booking Now button on confirm booking.");
			logger.log(Status.FAIL, MarkupHelper.createLabel("Failed to tap on Booking Now button on confirm booking.", ExtentColor.RED));

			//utils.takeScreenshot("Search", "SearchTest");
		}
	}

		@Test(priority=30)
		public void tapOnDONEButton() throws InterruptedException{
		Thread.sleep(1000);
			if(bdaScreen.tapOn_done_on_booking_confirmed())		
		{
				System.out.println("Clicked on DONE button on Booking Confirm Screen.");
				logger.log(Status.PASS, MarkupHelper.createLabel("Clicked on DONE button on Booking Confirm Screen.", ExtentColor.GREEN));
	
			}
			else
			{
				System.out.println("Failed to tap on DONE button on booking confirm  screen.");
				logger.log(Status.FAIL, MarkupHelper.createLabel("Failed to tap on DONE button on booking confirm  screen.", ExtentColor.RED));
	
				//utils.takeScreenshot("Search", "SearchTest");
			}
	
		if(restaurantDetailsPage.tapOnBackButton())
		{
			
			System.out.println("Clicked on Back Button");
			logger.log(Status.PASS, MarkupHelper.createLabel("Clicked on Back Button", ExtentColor.GREEN));
	
		}
		else
		{
			System.out.println("Back Button could not be clicked.");
			logger.log(Status.FAIL, MarkupHelper.createLabel("Failed to tap on back button.", ExtentColor.RED));
	
			//utils.takeScreenshot("Search", "SearchTest");
		}
		
	//report.endTest(logger);
	//report.flush();
	}
	
}
	

