package com.BookATable;

import com.BookATable.BaseTestInitializeClass;
import org.testng.annotations.Test;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;

public class TestToVerifyImages extends BaseTestInitializeClass {

	@Test(priority = 22)
	public void tapOnInfoTab() throws InterruptedException {

		// logger = report.startTest("TestToVerifyMaps");

		logger = report.createTest("Restaurant Details Page :tapOnInfoTab", "Verify Info tab.");
		if (restaurantDetailsPage.tapOnInfoTab()) {
			logger.log(Status.PASS,
					MarkupHelper.createLabel("Restaurant Details Page : Tapped on Info Tab from Restaurant detail Page.", ExtentColor.GREEN));

			System.out.println("Tapped on Info Tab from Restaurant detail Page.");
		} else {
			System.out.println("Failed to tap on Info Tab from Restaurant details page.");
			logger.log(Status.FAIL, MarkupHelper.createLabel("Failed to tap on Info Tab from Restaurant details page.",
					ExtentColor.RED));
			//utils.takeScreenshot("Search", "SearchTest");
		}
	}

	@Test(priority = 23)
	public void tapOnImageViewToFullScreen() throws InterruptedException {
		logger = report.createTest("Restaurant Details Page : tapOnImageViewToFullScreen", "Verify Image is open to full view.");
		if (restaurantDetailsPage.tapOnImageView()) {

			System.out.println("Tapped on Image View from the Info Tab of Restaurent Detail Page.");
			logger.log(Status.PASS, MarkupHelper.createLabel(
					"Tapped on Image View from the Info Tab of Restaurent Detail Page.", ExtentColor.GREEN));

		} else {
			System.out.println("Image view could not be clicked.");
			logger.log(Status.FAIL, MarkupHelper.createLabel("Image view could not be clicked.", ExtentColor.RED));
			//utils.takeScreenshot("Search", "SearchTest");
		}
	}

	@Test(priority = 24)
	public void swipeImage() throws InterruptedException {

		logger = report.createTest("Restaurant Details Page : swipeImage", "Verify Image swip functionality.");

		if (restaurantDetailsPage.swipeImages()) {
			System.out.println("Images Swipe was Successful.");
			logger.log(Status.PASS, MarkupHelper.createLabel("Images Swipe was Successful.", ExtentColor.GREEN));

		} else {
			System.out.println("Image Swipe was not successful.");
			logger.log(Status.FAIL, MarkupHelper.createLabel("Image Swipe was not successful.", ExtentColor.RED));
			//utils.takeScreenshot("Search", "SearchTest");
		}

	}

	@Test(priority = 25)
	public void tapOnBookNowButton() throws InterruptedException {
		logger = report.createTest("Restaurant Details Page : tapOnBookNowButton", "Verify Book Now Button on Image view.");

		if (restaurantDetailsPage.tapOnBookNowButtonfunction()) {
			System.out.println("Clicked on Book Now Button");
			logger.log(Status.PASS, MarkupHelper.createLabel("Clicked on Book Now Button", ExtentColor.GREEN));

		} else {
			logger.log(Status.FAIL, MarkupHelper.createLabel("Book Now Button could not be clicked.", ExtentColor.RED));
			System.out.println("Book Now Button could not be clicked.");
			//utils.takeScreenshot("Search", "SearchTest");
		}
	}

	@Test(priority = 26)
	public void tapOnCloseBDAScreen() throws InterruptedException {
		logger = report.createTest("BDA Screen : tapOnCloseBDAScreen", "Verify Close button on BDA Screen.");

		if (bdaScreen.tapOn_Close()) {
			System.out.println("Clicked on Close Button");
			logger.log(Status.PASS, MarkupHelper.createLabel("Clicked on Close Button", ExtentColor.GREEN));

		} else {
			System.out.println("Close Button could not be clicked.");
			logger.log(Status.FAIL, MarkupHelper.createLabel("Close Button could not be clicked.", ExtentColor.RED));
			//utils.takeScreenshot("Search", "SearchTest");
		}
	}

	@Test(priority = 27)
	public void tapOnDoneButtonOnFullView() throws InterruptedException {
		logger = report.createTest("Restaurant Details Page : tapOnDoneButtonOnFullView", "Verify Done button on Image full view.");

		if (restaurantDetailsPage.tapOnDoneButton()) {
			System.out.println("DONE button has been clicked.");
			logger.log(Status.PASS, MarkupHelper.createLabel("DONE button has been clicked.", ExtentColor.GREEN));

		} else {
			System.out.println("DONE button was not found.");
			logger.log(Status.FAIL, MarkupHelper.createLabel("DONE button was not found.", ExtentColor.RED));
			//utils.takeScreenshot("Search", "SearchTest");
		}
	}

	public void tapOnBackButtonOnDetailScreen() throws InterruptedException {
		logger = report.createTest("Restaurant Details Page : tapOnBackButtonOnDetailScreen", "Verify Back button on Details screen.");

		if (restaurantDetailsPage.tapOnBackButton()) {
			System.out.println("BACK button has been clicked.");
			logger.log(Status.PASS, MarkupHelper.createLabel("BACK button has been clicked.", ExtentColor.GREEN));

		} else {
			System.out.println("BACK button was not found.");
			logger.log(Status.FAIL, MarkupHelper.createLabel("DONE button was not found.", ExtentColor.RED));
			//utils.takeScreenshot("Search", "SearchTest");

		}
		// report.endTest(logger);
		 report.flush();
	}

}
