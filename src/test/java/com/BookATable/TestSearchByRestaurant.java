package com.BookATable;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
@Listeners(com.BookATable.ReportListener.class)	

public class TestSearchByRestaurant extends BaseTestInitializeClass {

	@Test(priority = 31)
	public void searchByRestaurant() throws InterruptedException {

		// logger = report.startTest("TestSearchByRestaurant");
		// splashScreenPage.permissionAllowButton();
		// logger.log(Status.INFO, "Test Search By Restaurant");
		logger = report.createTest("searchByRestaurant", "Verify functionality of search by restaurant.");

		System.out.println("Landed on Restaurant Listing Screen");

		if (restaurantNearMePage.searchTextbox() != null) {
			String search = "Cafe Ansari";
			restaurantNearMePage.searchTextbox().sendKeys(search);
			restaurantNearMePage.HideKeyboard();
			restaurantNearMePage.getLocationCountDropdownList();
			logger.log(Status.PASS,
					MarkupHelper.createLabel(
							"Search result found : " + search + " is " + +restaurantNearMePage.getDropdownCount(),
							ExtentColor.GREEN));

		} else {
			System.out.println("Go Button could not be clicked");
			logger.log(Status.FAIL, MarkupHelper.createLabel("Failed to enter search text.", ExtentColor.RED));
			//utils.takeScreenshot("Search", "SearchTest");
		}

	}

	@Test(priority = 32)
	public void selectRestaurant() throws InterruptedException {
		logger = report.createTest("selectRestaurant",
				"Verify functionality of selecting first restaurant from the list.");

		if (restaurantNearMePage.selectFirstRecordForRestaurantSearch()) {
			System.out.println("Landed on Restaurant Details Page");
			logger.log(Status.PASS, MarkupHelper.createLabel("Landed on Restaurant Details Page", ExtentColor.GREEN));

		} else {
			System.out.println("Restaurant could not be clicked from the list");
			logger.log(Status.FAIL,
					MarkupHelper.createLabel("Restaurant could not be clicked from the list", ExtentColor.RED));
			//utils.takeScreenshot("Search", "SearchTest");
		}
	}

	@Test(priority = 33)
	public void tapOnBookNowButton() throws InterruptedException {
		logger = report.createTest("tapOnBookNowButton", "Verify functionality of tap on Book Now button.");

		if (restaurantDetailsPage.tapOnBookNowButtonfunction()) {

			System.out.println("Diner has tapped on Book Now button.");
			logger.log(Status.PASS,
					MarkupHelper.createLabel("Diner has tapped on Book Now button.", ExtentColor.GREEN));

		} else {
			System.out.println("Failed to to tap on Book Now button.");
			logger.log(Status.FAIL, MarkupHelper.createLabel("Failed to to tap on Book Now button.", ExtentColor.RED));
			//utils.takeScreenshot("Search", "SearchTest");
		}

	}

	/*
	 * @Test public void selectNumberOfPeople() throws InterruptedException{
	 * if(bdaScreen.tapOnPeopleDropdown()) {
	 * System.out.println("Diner has selected number of people from dropdown.");
	 * logger.log(Status.PASS,
	 * "Diner has selected number of people from dropdown."); } else {
	 * System.out.
	 * println("Diner was not able to select number of people from the dropdown."
	 * ); logger.log(Status.FAIL,
	 * "Diner was not able to select number of people from the dropdown.");
	 * utils.takeScreenshot("Search", "SearchTest"); } }
	 * 
	 * @Test public void selectNumberOfPeoplefromList() throws
	 * InterruptedException{ if(bdaScreen.tapOnPeopleDropdownList()) {
	 * System.out.println("Diner has selected number of people from dropdown.");
	 * logger.log(Status.PASS,
	 * "Diner has selected number of people from dropdown."); } else {
	 * System.out.
	 * println("Diner was not able to select number of people from the dropdown."
	 * ); logger.log(Status.FAIL,
	 * "Diner was not able to select number of people from the dropdown.");
	 * utils.takeScreenshot("Search", "SearchTest"); } }
	 */

	@Test(priority = 33)
	public void selectCalendarDate() throws InterruptedException {

		// bdaScreen.tapOnPeopleDropdown();
		logger = report.createTest("selectCalendarDate", "Verify functionality of selection of date from calendar.");

		if (bdaScreen.selectCalendarDate()) {
			System.out.println("Diner has selected date from calendar.");
			logger.log(Status.PASS,
					MarkupHelper.createLabel("Diner has selected date from calendar.", ExtentColor.GREEN));

		} else {
			System.out.println("Diner was not able to select date from calendar.");
			logger.log(Status.FAIL,
					MarkupHelper.createLabel("Diner was not able to select date from calendar.", ExtentColor.RED));
			//utils.takeScreenshot("Search", "SearchTest");
		}
	}

	@Test(priority = 34)
	public void tapBookButton() throws InterruptedException {
		logger = report.createTest("tapBookButton", "Verify functionality of Book button on BDA screen.");

		if (bdaScreen.tapOnBookButton()) {

			System.out.println("Clicked on Book Button");
			logger.log(Status.PASS, MarkupHelper.createLabel("Clicked on Book Now Button.", ExtentColor.GREEN));

		} else {
			System.out.println("Book Button could not be clicked.");
			logger.log(Status.FAIL, MarkupHelper.createLabel("Failed to tap on book button.", ExtentColor.RED));
			//utils.takeScreenshot("Search", "SearchTest");
		}

		if (bdaScreen.selectTimeSlot()) {

			System.out.println("Time slot has been selected.");
			logger.log(Status.PASS, MarkupHelper.createLabel("Time slot has been selected.", ExtentColor.GREEN));

		} else {
			System.out.println("Time slot Button could not be clicked.");
			logger.log(Status.FAIL, MarkupHelper.createLabel("Failed to select the time slot.", ExtentColor.RED));
			//utils.takeScreenshot("Search", "SearchTest");
		}

	}

	// Book Now
	@Test(priority = 35)
	public void tapOnCloseButton() throws InterruptedException {
		if (bdaScreen.tapOn_Close()) {

			System.out.println("Clicked on Close Button");
			logger.log(Status.PASS, MarkupHelper.createLabel("Clicked on Close Button", ExtentColor.GREEN));

		} else {
			System.out.println("Close Button could not be clicked.");
			logger.log(Status.FAIL, MarkupHelper.createLabel("Failed to tap on Close button.", ExtentColor.RED));

			//utils.takeScreenshot("Search", "SearchTest");
		}
	}

	@Test(priority = 36)
	public void tapOnBackButton() throws InterruptedException {
		if (restaurantDetailsPage.tapOnBackButton()) {

			System.out.println("Clicked on Back Button");
			logger.log(Status.PASS, MarkupHelper.createLabel("Clicked on Back Button", ExtentColor.GREEN));

		} else {
			System.out.println("Back Button could not be clicked.");
			logger.log(Status.FAIL, MarkupHelper.createLabel("Failed to tap on back button.", ExtentColor.RED));

			//utils.takeScreenshot("Search", "SearchTest");
		}

		// report.endTest(logger);
		// report.flush();
	}

}
