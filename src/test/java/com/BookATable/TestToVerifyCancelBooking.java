package com.BookATable;

import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;

public class TestToVerifyCancelBooking extends BaseTestInitializeClass{
	
	@Test(priority=1)
	public void tapOnBookingTab() throws InterruptedException{
	
		//logger = report.startTest("Test to verify cancel booking.");
		logger.log(Status.INFO, "Test to verify cancel booking.");

		System.out.println("Landed on My Bookings Screen");
		
		if(myBookingsPage.tapOnBookingsTab())
		{
			System.out.println("Tapped on bookings tab from bottom.");
			logger.log(Status.PASS, "");

		}
		else
		{
			System.out.println("Failed to tap on bookings tab.");
			logger.log(Status.FAIL, "Failed to tap on bookings tab.");
			//utils.takeScreenshot("Search", "SearchTest");
		}
	
	if(myBookingsPage.tapOnPastTab())
	{
		System.out.println("Tapped on Past Tab in bookings page.");
		logger.log(Status.PASS, "");

	}
	else
	{
		System.out.println("Failed to tap on Past tab from booking page.");
		logger.log(Status.FAIL, "Failed to tap on Past tab from booking page.");
		//utils.takeScreenshot("Search", "SearchTest");
	}
	if(myBookingsPage.tapOnFindARestaurantButton())
	{
		System.out.println("Tapped on Find a restaurent button.");
		logger.log(Status.PASS, "");

	}
	else
	{
		System.out.println("Failed to tap on Find a restaurent button.");
		logger.log(Status.FAIL, "Failed to tap on Find a restaurent button.");
		//utils.takeScreenshot("Search", "SearchTest");
	}
	if(myBookingsPage.tapOnBookingsTab())
	{
		System.out.println("Tapped on bookings tab from bottom.");
		logger.log(Status.PASS, "");

	}
	else
	{
		System.out.println("Failed to tap on bookings tab.");
		logger.log(Status.FAIL, "Failed to tap on bookings tab.");
		//utils.takeScreenshot("Search", "SearchTest");
	}
	if(myBookingsPage.tapOnUpcomingTab())
	{
		System.out.println("Tapped on Upcoming Tab in bookings page.");
		logger.log(Status.PASS, "");

	}
	else
	{
		System.out.println("Failed to tap on Booking tab from booking page.");
		logger.log(Status.FAIL, "Failed to tap on Booking tab from booking page.");
		//utils.takeScreenshot("Search", "SearchTest");
	}
	if(myBookingsPage.selectFirstBookingRecord())
	{
		System.out.println("Tapped on first booked restaurant data from the booking list.");
		logger.log(Status.PASS, "");

	}
	else
	{
		System.out.println("Failed to tap on first booked restaurant data from the booking list.");
		logger.log(Status.FAIL, "Failed to tap on first booked restaurant data from the booking list.");
		//utils.takeScreenshot("Search", "SearchTest");
	}
	if(myBookingsPage.getBookedRestaurantName())
	{
		System.out.println(myBookingsPage.getBookedRestaurantName());
		logger.log(Status.PASS, "");

	}
	else
	{
		System.out.println("Failed to get name of restaurant.");
		logger.log(Status.FAIL, "Failed to get name of restaurant.");
		//utils.takeScreenshot("Search", "SearchTest");
	}
	if(myBookingsPage.getBookedRestaurantDetails())
	{
		System.out.println(myBookingsPage.getBookedRestaurantDetails());
		logger.log(Status.PASS, "");

	}
	else
	{
		System.out.println("Search Text was not entered.");
		logger.log(Status.FAIL, "Failed to enter search text.");
		//utils.takeScreenshot("Search", "SearchTest");
	}
	if(myBookingsPage.tapOnNextArrow())
	{
		System.out.println("Tapped on location from Location search Result.");
		logger.log(Status.PASS, "");

	}
	else
	{
		System.out.println("Search Text was not entered.");
		logger.log(Status.FAIL, "Failed to enter search text.");
		//utils.takeScreenshot("Search", "SearchTest");
	}
	if(myBookingsPage.getRestaurantNameInDetailsPage())
	{
		System.out.println(myBookingsPage.getRestaurantNameInDetailsPage());
		logger.log(Status.PASS, "");

	}
	else
	{
		System.out.println("Search Text was not entered.");
		logger.log(Status.FAIL, "Failed to enter search text.");
		//utils.takeScreenshot("Search", "SearchTest");
	}
	if(myBookingsPage.getPersonTextInDetailsPage())
	{
		System.out.println(myBookingsPage.getPersonTextInDetailsPage());
		logger.log(Status.PASS, "");

	}
	else
	{
		System.out.println("Search Text was not entered.");
		logger.log(Status.FAIL, "Failed to enter search text.");
		//utils.takeScreenshot("Search", "SearchTest");
	}
	if(myBookingsPage.getDateTextInDetailsPage())
	{
		System.out.println(myBookingsPage.getDateTextInDetailsPage());
		logger.log(Status.PASS, "");

	}
	else
	{
		System.out.println("Search Text was not entered.");
		logger.log(Status.FAIL, "Failed to enter search text.");
		//utils.takeScreenshot("Search", "SearchTest");
	}
	if(myBookingsPage.getTimeTextInDetailsPage())
	{
		System.out.println("Tapped on location from Location search Result.");
		logger.log(Status.PASS, "");

	}
	else
	{
		System.out.println("Search Text was not entered.");
		logger.log(Status.FAIL, "Failed to enter search text.");
		//utils.takeScreenshot("Search", "SearchTest");
	}
	if(myBookingsPage.tapOnCancelBookingsButton())
	{
		System.out.println("Tapped on location from Location search Result.");
		logger.log(Status.PASS, "");

	}
	else
	{
		System.out.println("Search Text was not entered.");
		logger.log(Status.FAIL, "Failed to enter search text.");
		//utils.takeScreenshot("Search", "SearchTest");
	}
}
}
