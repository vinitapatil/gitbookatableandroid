package com.BookATable;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import com.BookATable.BaseTestInitializeClass;
/**
 * @author Vinita Patil
 */
public class LocalizationConfigReader extends BaseTestInitializeClass{
	static Properties properties;
	
	public static void loadData() throws IOException {
		  properties = new Properties();		  
		  
		  File germanfile=new File("./src/mobileProperties/MessagesBundle_German.properties");
		  FileReader germanread=new FileReader(germanfile);
		  properties.load(germanread);		 
		  
		  /*File englishfile=new File(System.getProperty("user.dir")+"/src/mobileProperties/MessagesBundle_English-GB.properties");
		  FileReader englishread=new FileReader(englishfile);
		  properties.load(englishread);		  
		  
		  File mobileConfig=new File(System.getProperty("user.dir")+"/src/mobileProperties/MobileConfig.property");
		  FileReader mobileConfigread=new FileReader(mobileConfig);
		  properties.load(mobileConfigread);*/
	  }
	  
	  public static String getObject(String Data) throws IOException{
		  loadData();
		  String str =properties.getProperty(Data);
		return str;
		  
	  }
	  }

